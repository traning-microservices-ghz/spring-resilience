package com.example.springresilience;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringresilienceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringresilienceApplication.class, args);
	}

}
